﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [SerializeField] string playerName;

    [SerializeField] int maxLevel = 50;
    [SerializeField] int playerLevel = 1;
    [SerializeField] int currentXp;
    [SerializeField] int[] xpForNextLevel;
    [SerializeField] int baseLevelXP = 100;

    [SerializeField] int maxHP = 100;
    [SerializeField] int currentHp;

    [SerializeField] int maxMana = 30;
    [SerializeField] int currentMana;


    [SerializeField] int dexterity;
    [SerializeField] int defence;


    private void Start()
    {
        xpForNextLevel = new int[maxLevel];
        xpForNextLevel[1] = baseLevelXP;
        for(int i = 2; i < xpForNextLevel.Length; i++)
        {
            xpForNextLevel[i] = (int)(0.02f * i * i * i + 3.06f * i * i * i + 105.6f * i);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            AddXP(100);
        }
    }


    public void AddXP(int amountOfXP)
    {
        currentXp += amountOfXP;
        if(currentXp > xpForNextLevel[playerLevel])
        {
            currentXp -= xpForNextLevel[playerLevel];
            playerLevel++;

            if(playerLevel % 2 == 0)
            {
                dexterity++;
            }
            else
            {
                defence++;
            }

            maxHP = Mathf.FloorToInt(maxHP * 2f);
            currentHp = maxHP;

            maxMana = Mathf.FloorToInt(maxHP * 2f);
            currentMana = maxMana;

        }
    }
}
