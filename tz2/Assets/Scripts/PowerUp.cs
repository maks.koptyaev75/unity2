﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public GameFlow gameFlow;

    private void Start()
    {
        gameFlow = FindObjectOfType<GameFlow>();
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        collider.GetComponent<PlayerController>().doubleJump = true;
        gameFlow.StartCoroutine("FillAmount");
        Destroy(gameObject);
    }
}
