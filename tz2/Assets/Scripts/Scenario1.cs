﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario1 : MonoBehaviour
{
    
    private GameObject player;
    public Transform spawnPoint;

    private Key key;
    private Door door;


    private void Awake()
    {
        player = Resources.Load<GameObject>("Prefabs/Player");
        Instantiate(player, spawnPoint);

        key = FindObjectOfType<Key>();
        door = FindObjectOfType<Door>();
    }


    public void FadeDoor()
    {
        door.StartCoroutine("Fade");
    }


}
