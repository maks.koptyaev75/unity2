﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    Color _color;
    Renderer materialColor;
    [SerializeField] float count = 60;

    void Start()
    {
        materialColor = gameObject.GetComponent<Renderer>();
    }

    IEnumerator Fade()
    {
        for (float i = count; i > 0; i--)
        {
            _color = materialColor.material.color;
            if(_color.a > 0)
            {
                _color.a = i / count;
                materialColor.material.color = _color;
            }
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
        }
    
}
