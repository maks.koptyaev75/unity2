﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{

    private GameFlow gameFlow;

    private void Start()
    {
        gameFlow = FindObjectOfType<GameFlow>();
    }
    void OnTriggerEnter2D()
    {        
        if(gameFlow.levelNumber == 1)
        {
            gameFlow.LoadLevel(2);
            gameFlow.levelNumber = 2;
        }
        else if (gameFlow.levelNumber == 2)
        {
            gameFlow.LoadLevel(1);
            gameFlow.levelNumber = 1;
        }


    }


}
