﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameFlow : MonoBehaviour
{

    private CameraController cameraController;
    private GameObject _camera;
    
    public int levelNumber = 1;
    
    public GameObject levels;
    public GameObject cloneLevel;
    private bool _find = false;

    public Image powerUpBar;
    private PlayerController playerController;

    public static GameFlow instance { get; private set; }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            return;
        }
        Destroy(this.gameObject);
    }

    public void Start()
    {
        powerUpBar.fillAmount = 0;
        _camera = GameObject.FindGameObjectWithTag("MainCamera");
        cameraController = _camera.GetComponent<CameraController>();
        LoadLevel(levelNumber);
    }

    private void Update()
    {
        if (_find == false)
        {
            cameraController.FindPlayer();
            _find = true;
        }
    }



    public void LoadLevel(int levelNum)
    {
        if(playerController != null)
        {
            Destroy(playerController);
        }        
        Destroy(cloneLevel);
        levels = Resources.Load<GameObject>($"Prefabs/Level{levelNum}");
        cloneLevel = Instantiate(levels);
        playerController = FindObjectOfType<PlayerController>();
        _find = false;
        StopAllCoroutines();
        powerUpBar.fillAmount = 0;
        playerController.doubleJump = false;
    }

    IEnumerator FillAmount()
    {
        powerUpBar.fillAmount = 1;
        for (float i = 600; i > 0; i--)
        {
            powerUpBar.fillAmount = i / 600;
            yield return new WaitForSeconds(0.01f);
        }
        powerUpBar.fillAmount = 0;
        playerController.doubleJump = false;
    }



}
