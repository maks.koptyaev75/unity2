﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float jumpForce;
    private float moveInput;
    [SerializeField] bool isGrounded;
    public Transform groundCheck;
    public Vector2 checkRadius;
    public LayerMask whatIsGround;
    public bool doubleJump = false;
    public GameFlow gameFlow;
    [SerializeField] int jumpCount = 2;
    


    private Rigidbody2D rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        gameFlow = FindObjectOfType<GameFlow>();
    }


    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapBox(groundCheck.position, checkRadius, whatIsGround);      
    }


    private void Update()
    {
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded == true)
        {            
            rb.velocity = Vector2.up * jumpForce;
        }        
        else if (Input.GetKeyDown(KeyCode.Space) && doubleJump == true && isGrounded == false && jumpCount >= 2)
        {
            rb.velocity = Vector2.up * jumpForce;
            jumpCount--;
        }
        if (isGrounded == true && doubleJump == true)
        {
            //doubleJumpPossibillity = false;
            jumpCount = 2;
        }
        //if (jumpCount == 1 || gameFlow.powerUpBar.fillAmount == 0)
        //{
        //    doubleJump = false;
        //    gameFlow.StopAllCoroutines();
        //    gameFlow.powerUpBar.fillAmount = 0;
        //}

    }
}
