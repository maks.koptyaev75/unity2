﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario2 : MonoBehaviour
{
    private GameObject player;
    public Transform spawnPoint;
    public Key[] keysScript = new Key[2];
    public Door[] doors = new Door[2];

    private void Awake()
    {
        player = Resources.Load<GameObject>("Prefabs/Player");
        Instantiate(player, spawnPoint);
        keysScript = FindObjectsOfType<Key>();
        doors = FindObjectsOfType<Door>();
    }

    public void FadeDoor(int doorNum)
    {   
         doors[doorNum].StartCoroutine("Fade");
    }

}
