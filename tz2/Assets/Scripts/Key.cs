﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public bool isTaken;
    private Scenario2 scenario2;
    private Scenario1 scenario1;
    
    private void Start()
    {
        scenario2 = FindObjectOfType<Scenario2>();
        scenario1 = FindObjectOfType<Scenario1>();
}

    void OnTriggerEnter2D()
    {
        isTaken = true;
        Destroy(gameObject);
        if (scenario2 != null)
        {
            if (gameObject.tag == "BlueKey")
            {
                scenario2.FadeDoor(1);
            }
            if (gameObject.tag == "RedKey")
            {
                scenario2.FadeDoor(0);
            }
        }
        else
        {
            scenario1.FadeDoor();
        }
        
    }
}
